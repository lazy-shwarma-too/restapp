from flask import Flask
from flask import jsonify
from functions import *

app =  Flask(__name__)
app.config.from_object=('config')

@app.route('/')
def index():
    return jsonify( success= True), 200

@app.route('/stats')
def stats():
   return platform_stats()

