import os
import platform


def platform_stats():
    return [platform.mac_ver(), platform.node(), platform.system(), platform.release()]
    
